---
# Display name
title: Thingspire

# Username (this should match the folder name)
authors:
- thingspire

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: Thingspire
  url: "https://www.thingspire.com/"

user_groups:
- "협력사"

---

![web](./web.png)