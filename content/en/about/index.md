---
title: About Us
---
Future ICT is a technology group develops, produces and sells [sensor nodes and gateways](/en/products), implementing connectivity for Industrial IoT ("Industry 4.0") applications.

Future ICT is an [enginneering consultancy](/en/post) company specialized in prototyping IoT devices, as a reference, tailored to specific [industrial application](/en/publication) contexts.

The company was founded in 2018 by a few of experts in the embedded fields for secure communication.

### Our engineering efforts are mainly aimed at improving:
  - **Security** in wireless communication
  - **Reliability** in harsh environments
  - **Openness** in platforms
