---
# Course title, summary, and position.
linktitle: IoT Gateways & Sensor Nodes
summary: Simplify the networking of things
weight: 1

# Page metadata.
title: IoT Gateways & Sensor Nodes
date: "2018-09-09T00:00:00Z"
lastmod: "2018-09-09T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  IoT:
    name: IoT Gateways & Sensor Nodes
    weight: 1
---

