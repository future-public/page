---
# Display name
title: LG화학

# Username (this should match the folder name)
authors:
- lgchem

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: LG화학
  url: "https://www.lgchem.com/"

user_groups:
- "주요 고객사"

---

![web](./web.png)