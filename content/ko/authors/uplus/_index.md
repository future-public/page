---
# Display name
title: (주)엘지유플러스

# Username (this should match the folder name)
authors:
- uplus

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: LG U+
  url: "https://www.uplus.co.kr/"

user_groups:
- "주요 고객사"

---

![web](./web.png)