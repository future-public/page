---
title: FDL20-TH
linktitle:
toc: true
type: docs
summary: LoRa 통신 기반의 배터리 운영이 가능한 저전력 온도/습도 디바이스
tags:
- IoT Sensor Node
- LoRa
date: "2020-07-18T12:33:46+10:00"

# Optional external URL for project (replaces project detail page).
external_link: ""

menu:
  IoT:
    parent: IoT Sensor Nodes
    weight: 1
#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![FDL20-TH](./featured.jpg)

> - LoRa 통신 기반의 배터리 운영이 가능한 저전력 디바이스.
> - 디지털 센서의 세계적 선두 기업인 Sensirion 사의 고정밀 디지털 온/습도 센서 적용.
> - 배터리 운영을 통한 간편한 설치와 고객에 맞춘 최적화 작업을 통해 기존 설비에 데이터 수집 지원.

## Key Features

- Ultra Low Power & High Performance Microcontroller
- Supprt LoRaWAN 1.0.2 (Class A/C)
- Frequency : 902 ~ 958MHz
- AES-128 encryption/decryption
- Max Current: 30~40mA(TX, 10dbm), 10mA(Listen), 10uA(Sleep)
  
## Applications

- Wireless sensor Network and Security System.
- Smart Utility.
- Smart Metering.
- Home and Building Automation.
- Process and Building Control.  

## Sensor Specifications

|항목|사양|
|---|---|
|Vendor|Sensirion|
|Model|SHT3x|
|T operating range|-40 to +125°C (-40 to +257°F)|
|T Accuracy|± 0.3°C (@0-65 °C)|
|RH operating range|0 - 100% RH|
|RH Accuracy|± 3% RH (@10-90% RH)|
|RH response time|8 sec (tau63%)|
|Size|2.5 x 2.5 x 0.9 mm|
|Output|I²C, Voltage Out|
|Supply voltage range|2.15 to 5.5 V|
|Energy consumption|4.8µW (at 2.4 V, low repeatability, 1 measurement / s)|

## Device Specifications

|항목||내용|비고|
|---|---|:-:|---|
|CPU|Cortex-M0|32MHz||
|Memory|RAM|20KB||
||Flash|192KB||
|LoRa|Class|Class A, Class C||
||RF Region|KR920|
||RF Power|14dBm|
|Others|Operating temperature|-20℃ ~ 60℃| |
||Storage temperature|-40℃ ~ 80℃| |
||Dimensions|||
||Power|Lithium Battery 3.6V (D Size)||
