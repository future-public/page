---
# Course title, summary, and position.
linktitle: 제품 자료
summary: 퓨쳐아이씨티에서 공급하는 제품들에 대해 자료를 제공합니다.
weight: 2

# Page metadata.
title: 제품 자료
date: "2018-09-09T00:00:00Z"
lastmod: "2018-09-09T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: # docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  example:
    name: 제품 자료
    weight: 2
---

> 퓨쳐아이씨티에서 판매되고 있는 제품들에 대한 자료 제공

---
## IoT 장비

* [**IoT 장비 소개 2020 v1.0.0**](./IoT_장비소개자료_퓨쳐ICT(2020)_v1.pdf)

#### FML50S

* [**FML50S 제품 소개서 v1.0.0**](./FutureICT_FML50S_제품소개서_v1.pdf)
* [**LTE Router WEB Admin Manual v1.0.2**](./퓨쳐ICT_LTE_ROUTER-WEB_ADMIN_MANUAL_V1.0.2.pdf)

#### FTM80

* [**FTM80 제품 소개서 v1.0.0**](./FutureICT_FTM80_제품소개서_v1.pdf)

#### FTM100U

* [**FTM100U 제품 소개서 v1.0.0**](./FutureICT_FTM100U_제품소개서_v1.pdf)

---

## VPN 전용 장비

#### SSLplus110

* [**VPN 제안서 SSLplus110**](./Futureict_VPN_제안서_SSLplus110.pdf)

#### SSLplus1100

* [**VPN 제안서 SSLplus1100**](./Futureict_VPN_제안서_SSLplus1100.pdf)
