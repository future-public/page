---
# Display name
title: 현대제철

# Username (this should match the folder name)
authors:
- hyundaisteel
- 

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 현대제철
  url: "https://www.hyundai-steel.com"

user_groups:
- "주요 고객사"

---
![web](./web.png)