---
title: TIG
linktitle: TIG
toc: true
type: docs
summary: Open source based TIG Stack
tags:
#- IoT Gateway
#- LTE
date: "2016-04-27T00:00:00Z"

menu:
  Platform:
    parent: Service Platform
    weight:  1

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![grafana](./grafana.png)

> - **T**elegraf - InfluxData에서 제작한 시스템 모니터링 및 지표 수집 에이전트 
> - **I**nfluxDB - InfluxData가 개발한 오픈 소스 시계열 데이터베이스 
> - **G**rafana -  다중 플랫폼 오픈 소스 분석 및 대화 형 시각화 웹 응용 프로그램

## General

