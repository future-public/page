---
# Display name
title: LG CNS

# Username (this should match the folder name)
authors:
- lgcns
- 

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: LG CNS
  url: "https://www.lgcns.com/"

user_groups:
- "주요 고객사"

---

![web](./web.png)