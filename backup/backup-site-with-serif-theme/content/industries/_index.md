---
title: 'Industries'
intro_image: images/future/banners/industries.svg
intro_image_absolute: true # edit /assets/scss/components/_intro-image.scss for full control
intro_image_hide_on_mobile: true
---

# Internet of Things
## Bridge the gap between devices and services
