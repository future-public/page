---
title: 'Smart Environment Monitoring Service'
date: 2018-11-28T15:15:26+10:00
draft: false
weight: 1
---

주변 환경 데이터를 수집을 통해 환경 변화를 감시하고, 변화에 대한 위험적 요소가 발견될 경우, 이에 대해 신속하고 적극적인 대응이 가능하도록 지원함으로써, 사고를 예방하고 피해를 감소 시킴.

## 고려 사항

## 구성 방안

## 적용 예
