---
title: 'Smart Environment Monitoring Service'
authors:
- admin
date: "2019-04-07T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: 주변 환경 데이터를 수집을 통해 환경 변화를 감시하고, 변화에 대한 위험적 요소가 발견될 경우, 이에 대해 신속하고 적극적인 대응이 가능하도록 지원함으로써, 사고를 예방하고 피해를 감소 시킴.

# Summary. An optional shortened abstract.
summary: 주변 환경 데이터를 수집을 통해 환경 변화를 감시하고, 변화에 대한 위험적 요소가 발견될 경우, 이에 대해 신속하고 적극적인 대응이 가능하도록 지원함으로써, 사고를 예방하고 피해를 감소 시킴.

tags:
- Source Themes
featured: false

links:
- name: Custom Link
  url: http://example.org
url_pdf: http://arxiv.org/pdf/1512.04133v1
url_code: '#'
url_dataset: '#'
url_poster: '#'
url_project: ''
url_slides: ''
url_source: '#'
url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: example
---

주변 환경 데이터를 수집을 통해 환경 변화를 감시하고, 변화에 대한 위험적 요소가 발견될 경우, 이에 대해 신속하고 적극적인 대응이 가능하도록 지원함으로써, 사고를 예방하고 피해를 감소 시킴.

## 고려 사항

## 구성 방안

## 적용 예
