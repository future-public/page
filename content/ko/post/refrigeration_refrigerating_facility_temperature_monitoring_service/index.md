---
title: "M사 냉장/냉동 설비 온도 모니터링 서비스"
date: 2019-07-12
math: true
diagram: true
weight: 2
image:
  placement: 3
  caption: 'Image credit: [**chuttersnap**](https://unsplash.com/photos/BNBA1h-NgdY)'

---

> - 국내 유명 온라인 커머스에서 운영 중인 냉동/냉장 설비에 대한 온도 모니터링 서비스
> - 차별화된 배송 서비스로 대중적인 관심을 끌고, 비대면 시대의 새롭게 각광받고 있는 M사의 냉장/냉동 설비 온도 모니터링 서비스 구축

---

## 서비스 구성

![구성](./architecture.svg)

## 대시보드

![대시보드](./dashboard.png)

- 센서 그룹 관리
- 시계열 데이터 그래프

![대시보드](./dashboard2.png)

- 룰 기반 알람 설정

## 적용 기술

### LPWA 통신 기술 적용

- 신규 설비 도입에서 많은 비중은 차지하는 설치비를 최소화 하기 위해 무선 기술 적용
- 유지비 최소화를 위한 사설 무선망 구축

### LoRa to LTE 게이트웨이

- LoRa Gateway 기능이 탑재된 LTE 게이트웨이 FTM80 적용

### AWS EC2 서비스상에 구축

- M사에서 기존 서비스 구축 후 운영 중인 AWS 상에 모니터링 서버 구축
- 서버 안정화를 위한 Active-Active 이중화

## 디바이스 구성

### FDL20-T

![FDL20-T](../../project/fdl20-t/fdl20-t.jpg)

- LoRaWAN 기반 온도 센서 디바이스
- 배터리 운영 가능

### FTM80-LoRa

![FTM80-LoRa](../../project/ftm80-lora/ftm80-lora.jpg)

- 고성능 ARM 프로세서를 탑재한 LoRa 게이트웨이
- LG U+ LTE 무선통신 지원
- LoRa 8채널 동시 지원
- LoRa Network Server 탑재
- 자체 개발 프로그램 탑재 가능