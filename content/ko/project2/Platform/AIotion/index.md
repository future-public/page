---
title: AIOTION
linktitle: AIOTION
toc: true
type: docs
summary: NKIA
tags:
#- IoT Gateway
#- LTE
date: "2018-11-01T00:00:00Z"

menu:
  Platform:
    parent: Service Platform
    weight: 3

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

![AIOtion](./aiotion.png)

> - AIOTION analyzes data collected from various sensors and related systems and displays the results in various perspectives.
> - AIOTION can be applied to smart factories, roads, railways, ports, airports, smart building, smart offices, and smart cities with various sensors and equipment monitoring

## General

