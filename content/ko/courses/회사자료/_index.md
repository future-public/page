---
# Course title, summary, and position.
linktitle: 영업 자료
summary: 퓨쳐야이씨티의 소개 및 솔루션 관련 자료를 제공합니다
weight: 1

# Page metadata.
title: 영업 자료
date: "2018-09-09T00:00:00Z"
lastmod: "2018-09-09T00:00:00Z"
draft: false  # Is this a draft? true/false
toc: true  # Show table of contents? true/false
type: # docs  # Do not modify.

# Add menu entry to sidebar.
# - name: Declare this menu item as a parent with ID `name`.
# - weight: Position of link in menu.
menu:
  example:
    name: 영업 자료
    weight: 1
---

> - 퓨쳐아이씨티를 소개합니다.
> - 고객이 만족할 수 있는 최상의 솔루션을 제안 드리겠습니다.

### 회사 자료

* [**회사 소개서**](./퓨쳐ICT_회사소개자료_IoT_2020_v1.pdf)

### 솔루션 제안서

* [**LoRa 솔루션**](./futureict_lora_solution_v1_0_1.pdf)
