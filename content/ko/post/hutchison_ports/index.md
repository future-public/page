---
title: "H사 항만물류 시스템을 위한 Private-LTE 무선 사설망 구축"
date: 2020-08-01
math: true
diagram: true
weight: 1
image:
  placement: 3
  caption: 'Image credit: [**Ronan Furuta**](https://unsplash.com/photos/daSq9zhROxY)'
---

> 항만 터미널을 운영하고 있는 H사에서는 기존에 사용하던 WiFi기반의 운영망을 Private-LTE 기반으로 전환하여 보다 안정적인 터미널 운영을 유지함

---

## 서비스 구성

![망구성도](./architecture.svg)

## 적용 기술

### Private-LTE

- 무선망의 특성상 공유될 수 있는 외부의 영향을 최소화 하기 위해 LTE 환경하에서 별도의 망구성이 가능한 Privatre-LTE 서비스 적용
- 특정 서비스별로 APN 관리
- 해당 APN에 등록되어 있는 장비만이 해당 망에 접속 가능

### SSLVPN

- SSL Tunneling을 통해 독립적인 망 구성
- 암호화된 패킷으로 데이터 보호
- 통신사 부가 서비스인 공인 고정 IP를 사용하지 않더라도 사설망 내에서 고정 IP 운영 가능. (통신사에서 제공하는 고정 IP가 아니므로 별도의 요금 지불이 없음)
- CC(EAL4) 인증 획득 제품으로 신뢰성 보장

## 적용 디바이스

### FML50s

![FML50s](../../project/fml50s/fml50s.jpg)

- LTE 기반 무선 게이트웨이
- SSLVEN Client 탑재

### Weguardia SSLPlus

- CC(EAL4) 인증 획득
- 라우터 접속 관리
