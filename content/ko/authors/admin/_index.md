---
# Display name
# title: Future ICT

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: 

# Organizations/Affiliations
organizations:
- name:
  url: ""

# Short bio (displayed in user profile at end of posts)

# interests:
# - Artificial Intelligence
#- Computational Linguistics
#- Information Retrieval
#
#education:
#  courses:
#  - course: PhD in Artificial Intelligence
#    institution: Stanford University
#    year: 2012
#  - course: MEng in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2009
#  - course: BSc in Artificial Intelligence
#    institution: Massachusetts Institute of Technology
#    year: 2008

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
#- icon: envelope
#  icon_pack: fas
#  link: '#contact'  # For a direct email link, use "mailto:test@example.org".
#- icon: twitter
#  icon_pack: fab
#  link: https://twitter.com/GeorgeCushen
#- icon: google-scholar
#  icon_pack: ai
#  link: https://scholar.google.co.uk/citations?user=sIwtMXoAAAAJ
#- icon: gitlab
#  icon_pack: fab
#  link: https://github.com/gcushen
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: files/cv.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
user_groups:
#- Researchers
#- Visitors
---

<!-- FutureICT는 Industrial IoT("Industry 4.0")에서 요구하는 다양한 연결 제품과 이를 통한 서비스를 개발 공급합니다.

FutureICT는 2018년 임베디드 전문가들로 구성되어 설립되었으며, IoT 서비스에 관련된 디바이스 및 서비스 개발에 특화되어 있는 전문가 기술 그룹으로 고객이 원하는 기술적 해결 방안을 제시해 줄 수 있습니다.

### 우리는 다음을 목표로 나아가고 있습니다.

- 무선 통신에서의 **보안**
- 가혹한 운영 환경에서의 **신뢰성**
- 플랫폼에서의 **개방성**
-->

## 개요

㈜퓨쳐아이씨티는  혁신적인 IoT기반의  End-to-End 솔루션 개발사로서 수도, 전기, 가스와 같은 스마트 유틸리티 ,  IndustrialIoT, Smart Farm, 多호환성 기반 서비스用 M2M / IoT 솔루션 개발 및 제조사입니다

## CI

- 무선 통신에서의 **보안**
- 가혹한 운영 환경에서의 **신뢰성**
- 플랫폼에서의 **개방성**
 
## 주요 연혁

### 2023

- **12월 -** 경찰청 전산실 NMS 센서망 구축 수주
- **12월 -** 롯데캐미칼 우수로 오염물질 조기감지시스템 구축(Y2엔지니어링)
- **11월 -** 부산신항 IoT센서망 구축 2사업 진행(Private LoRa)
- **09월 -** 부산시 지역혁신 자율과제 선정(신라대)
- **08월 -** 베어즈베스트CC 저수지 펌프관제시스템 구축
- **05월 -** Rack형 Sensor Gateway 개발(LoRaWan 지원)

### 2022

- **12월 -** 부산신항 IoT센서망 구축(Private LoRa)
- **10월 -** DN솔루션즈 공작기계 관제시스템 구축
- **09월 -** 노벨리스코리아 스팀트랩 표면온도 모니터링시스템 구축(Private LoRa)
- **08월 -** 롯데칠성 스팀트랩 표면온도 모니터링시스템 구축(Private LoRa)
- **06월 -** 도로공사 신축교량 이음장치 관리시스템 개발(Private LoRa)

### 2021

- **12월 -** 부산 BIDC IoT 누유관시 시스템 구축
- **11월 -** 산자부 규제샌드박스 과제 선정
- **09월 -** 유한킴벌리 용수모니터링 구축
- **05월 -** 부설 정보통신연구소 설립
- **04월 -** 농기평 2021년 농업기반 및 재해대응 기술개발 과제 선정
- **04월 -** 부산지사 설립
- **03월 -** 우이신설선 공기청정기 관제시스템 구축
- **01월 -** 써닝포인트CC 골프장 시설관제시스템 구축

### 2020

- **08월 -** Lora Gateway “UC11-N1” KC인증
- **06월 -** 한진부산컨테이너터미널 LTE VPN Router 구축
- **05월 -** NIPA 2020년 융합제품 상용화 과제 선정
- **03월 -** 한화토탈 Smart Plant 솔루션 구축 2차(유체누출 )
- **02월 -** 전남대 KORUS 온/습도 모니터링 시스템구축(Private Lora)
- **01월 -** 두산인프라코어(인천) 온/습도 모니터링 시스템구축(Private Lora)

### 2019

- **12월 -**  르노삼성자동차 AGV모니터링 시스템용 LTE VPN Router 구축
- **11월 -**  서울대 의과대학 설비관제시스템용 LTE VPN Router 구축
- **10월 -**  산업용 LTE Router “FML50S” 개발(LG U+ 사용성 인증)
- **08월 -**  마켓컬리 물류창고  온습도 모니터링 시스템 구축 
- **04월 -**  롯데워터파크(김해) 수질모니터링 시스템구축
- **03월 -**  삼양사 IoT지진 모니터링시스템 구축
- **01월 -**  농정원 스마트팜 빅테이터 확산사업 진행(딸기농장 100개소 스마트팜 구축)

### 2018

- **12월 -**  한화토탈 Smart Plant 솔루션 구축(유체, 가스, 유량)
- **12월 -**  두산중공업(부산) 전력 원격검침 시스템구축(NB-IoT)
- **11월 -**  M2M/IoT 전문기업인 “㈜퓨쳐텍  IoT사업부”가 분사하여 (주)퓨쳐ICT 설립
