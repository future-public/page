---
# Display name
title: 제이원아이티

# Username (this should match the folder name)
authors:
- jwonit

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 제이원아이티
  url: "https://www.jwonit.com/"

user_groups:
- "협력사"

---

![web](./web.png)