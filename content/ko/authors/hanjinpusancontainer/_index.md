---
# Display name
title: 한진부산컨테이너터미널(주)

# Username (this should match the folder name)
authors:
- hanjincontainer

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 한진부산컨테이너터미널
  url: "https://www.hjnc.co.kr/"

user_groups:
- ""

---

![web](./web.png)
