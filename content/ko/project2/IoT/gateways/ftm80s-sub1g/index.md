---
title: FTM80s-Sub1G
linktitle:
toc: true
type: docs
summary: 고성능 ARM 프로세서를 탑재하고 독립 운영이 가능한 Sub-1GHz 게이트웨이 
tags:
- IoT Gateway
date: "2016-04-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

menu:
  IoT:
    parent: IoT Gateways
    weight: 1
#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""
#
# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![ftm80s-lora](./featured.jpg)

> - 고성능 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터
> - Sub-1GHz 1채널 Concentrator를 내장하고 있는 무선-to-무선 게이트웨이

## Functions

### General

- Low power high performace CPU
- Linux support
- LTE Router
(LG U+ Certified)

### Networks

- Wired/Wireless failover
- Port fowarding
- Support for multiple protocols
  (TCP, UDP, SNMP, MQTT, HTTP,...)

### Management

- Web-based management tool
- Self reboot
(after checking communication status)
- Cloud-based health monitoring
(optional support)

### Security

- Netfilter
- SSLVPN client (Options)
(CC EAL4 certified)

### Sub-1GHz Gateway

- 1 channel 

## Specifications

|항목||내용|비고|
|-|-|:-:|---|
|CPU|Cortex-A8|1GHz||
|Memory|RAM|DDR3 512MB||
||Flash|SDHC 8GB ||
|Interface| LTE|B1/B5/B7||
|| 10/100/1000 Ethernet |2||
|| RS232 |1||
|| RS485 |1||
|| Analog Input|2||
|| Digial Input|2||
||Sub-1GHz Concentrator|TI Sub-1GHz SimpleLink||
|Others|Operating temperature|-20 ~ 60||
||Storage temperature|-40 ~ 80||
||Power|12V ~ 24V DC||
||Dimensions|||
