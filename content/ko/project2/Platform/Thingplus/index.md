---
title: Thingplus
linktitle: Thingplus
toc: true
type: docs
summary: Daliworks
tags:
#- IoT Gateway
#- LTE
date: "2016-04-27T00:00:00Z"

menu:
  Platform:
    parent: Service Platform
    weight:  2

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![Thingplus](./thingplus.png)

> - Thing+는 IoT 서비스를 가장 빠르고 쉽게 사용할 수 있도록 별도의 설치 및 구축 없이 웹 기반으로 서비스를 제공하는 IoT Cloud Platform 입니다
> - 사물(Thing)은 환경, 설비, 중장비, 원격 자산 및 차량에 센서, 칩 및 태그를 포함시키는 간단한 개념이며, 이 사물인터넷에 인터넷 연결을 설정하고 인터넷을 통해 데이터를 전송합니다.
> - 진정한 End to End IoT플랫폼은 모든 ‘사물’을 원격으로 연결하고, 장치를 관리하고, 데이터를 수집하고, 작업 관리, 분석 및 시각화를 수행하고, 클라우드 서비스와 통합하는 소프트웨어 프레임 워크입니다.
> - <출처 : http://www.daliworks.net/thing-plus/>