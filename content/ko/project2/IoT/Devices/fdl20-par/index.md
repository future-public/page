---
title: FDL20-PAR
linktitle:
toc: true
type: docs
summary: LoRa 통신 기반의 배터리 운영이 가능한 저전력 퀀텀 센서 디바이스
tags:
- IoT Sensor Node
- LoRa
date: "2020-07-18T12:33:46+10:00"

# Optional external URL for project (replaces project detail page).
external_link: ""

menu:
  IoT:
    parent: IoT Sensor Nodes
    weight: 1
#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![FDL20-PAR](./featured.jpg)

> - LoRa 통신 기반의 배터리 운영이 가능한 저전력 디바이스.
> - 세계적인 환경 센서 선두 기업이 Apogee Instruments사의 Quantum 센서 적용.
> - 배터리 운영을 통한 간편한 설치와 고객에 맞춘 최적화 작업을 통해 기존 설비에 데이터 수집 지원.

## Key Features

- Ultra Low Power & High Performance Microcontroller
- Supprt LoRaWAN 1.0.2 (Class A/C)
- Frequency : 902 ~ 958MHz
- AES-128 encryption/decryption
- Max Current: 30~40mA(TX, 10dbm), 10mA(Listen), 10uA(Sleep)
  
## Applications

- Smart Farm.
- Environment Monitoring.

## Quantum Sensor Specifications

||Specification|
|---|---|
|Vendor|Apogee Instruments|
|Model|SQ-215|
| Sensitivity| 2 mV per µmol mˉ² sˉ¹
| Calibration Factor|0.5 µmol mˉ² sˉ¹ per mV (reciprocal of sensitivity)
| Calibration Uncertainty| ± 5 %
| Calibrated Output Range| 0 to 5 V
| Measurement Repeatability| Less than 0.5 %
| Non-stability (Long-term Drift)| Less than 2 % per year
| Non-linearity|Less than 1 % (up to 2500 µmol mˉ² sˉ¹; maximum PPFD measurement is 2500 µmol mˉ² sˉ¹)
| Response Time|Less than 1 ms
| Field of View| 180˚
|Spectral Range|410 nm to 655 nm (wavelengths where response is greater than 50 % of maximum)
|Spectral Selectivity|Less than 10 % from 469 to 655 nm
|Directional (Cosine) Response|± 5 % at 75˚ zenith angle|
|Temperature Response|0.06 ± 0.06 % per C|
|Operating Environment|-40 to 70 C; 0 to 100 % relative humidity; can be submerged in water up to depths of 30 m|
|Dimensions|24 mm diameter, 33 mm height
|Mass|100 g (with 5 m of lead wire)

## Device Specifications

|항목||내용|비고|
|---|---|:-:|---|
|CPU|Cortex-M0|32MHz||
|Memory|RAM|20KB||
||Flash|192KB||
|LoRa|Class|Class A, Class C||
||RF Region|KR920|
||RF Power|14dBm|
|Others|Operating temperature|-20℃ ~ 60℃| |
||Storage temperature|-40℃ ~ 80℃| |
||Dimensions|||
||Power|Lithium Battery 3.6V (D Size)||
