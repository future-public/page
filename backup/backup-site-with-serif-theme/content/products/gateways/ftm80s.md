---
title: 'FTM80'
date: 2020-07-18T12:33:46+10:00
draft: false
featured: true
weight: 1
---


고성능 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터  
FTM80S에서는 2개의 확장 슬롯이 지원되며, 확장 슬롯에는 산업 현장에 맞는 다양한 인터페이스 모듈이 연결 가능  
인터페이스 모듈은 유선 뿐만 아니라 무선 통신을  사설 무선망 구성이 가능  

![FTM80](/images/future/ftm50s.jpg)

## Functions

### General

* Low power high performace CPU
* Linux support
* LTE Router  
(LG U+ Certified)

### Networks

* Wired/Wireless failover
* Port fowarding
* Support for multiple protocols  
  (TCP, UDP, SNMP, MQTT, HTTP,...)

### Management

* Web-based management tool
* Self reboot  
(after checking communication status)
* Cloud-based health monitoring  
(optional support)

### Security

* Netfilter
* SSLVPN client (Options)  
(CC EAL4 certified)

### Expantion Module

* LoRa Concentrator  
(LoRa Gateway configuable)
* Digital Input/Output  
(Digital Input/Output 8 ports)

### LoRa Gateway (option)

* Up to 8 simultaneous channels
* Compliance to LoRaWAN Class A and Class C
* Integrated with multiple network server(NS)

### User-required programs can be loaded

* External device interworking  
(Ethernet, RS232, RS485, Analog Input)
* Data Collection form device
* Tranfering data to a specified server
(MQTT, HTTP, user defined protocol)
 
## Specifications

|항목||내용|비고|
|-|-|:-:|---|
|CPU|Cortex-A8|1GHz||
|Memory|RAM|DDR3 512MB||
||Flash|SDHC 8GB ||
|Interface| LTE|B1/B5/B7||
|| 10/100/1000 Ethernet |2||
|| RS232 |1||
|| RS485 |1||
|| Analog Input|2||
|| Digial Input|2||
|Expantion Module| Digital I/O|4||
||Analog Input|4|To be supported|
||LoRa Concentrator|SETALab module support|KC certified|
|LoRaWAN|Channel|8||
||Freqency Band|KR920,US902,AU915,AS923 and so on||
||Sensitivity|-140dBm @ 292bps||
||Protocol|V1.0 class A/C and V1.0.2 class A/C||
|Others|Operating temperature|-20 ~ 60||
||Storage temperature|-40 ~ 80||
||Power|12V ~ 24V DC||
||Dimensions|||
