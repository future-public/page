---
title: 스마트 환경 감시
authors:
- xtra
date: "2020-08-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-08-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

#abstract: 주변 환경 데이터를 수집을 통해 환경 변화를 감시하고, 변화에 대한 위험적 요소가 발견될 경우, 이에 대해 신속하고 적극적인 대응이 가능하도록 지원함으로써, 사고를 예방하고 피해를 감소 시킴.

# Summary. An optional shortened abstract.
summary: 주변 환경 데이터를 수집을 통해 환경 변화를 감시하고, 변화에 대한 위험적 요소가 발견될 경우, 이에 대해 신속하고 적극적인 대응이 가능하도록 지원함으로써, 사고를 예방하고 피해를 감소 시킴.

tags:
#- Source Themes
featured: false

#links1:
#- name: Custom Link
#  url: http://example.org
#url_pdf: http://arxiv.org/pdf/1512.04133v1
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/H72SCCTZPE8)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: example
---


## 스마트 환경 감시란?

환경 IoT 센서를 통해 수집된 데이터를 가공해 환경 정보를 제공하고, 지속적인 데이터 변화에 대한 분석을 통해 예측 가능한 변화를 추출하고 이를 통한 사고의 사전 예방을 목적으로 함.

IT 기술의 발달로 각종 센서에 대한 접근성이 낮아짐. 몇 년 전만 하더라도 특정한 환경 정보를 수집하기 위해서는 고가의 센서와 이를 운영하기 위한 센서 디바이스가 요구되었지만, 기술적 발달은 이러한 센서나 디바이스에 대한 접근성을 현저히 낮추는 성과를 가져옴.
이에 따라, 환경 데이터 수집을 위한 센서 디바이스는 과거와는 비교할 수 없을 만큼 저렴하게 떨어지고, 센서 설치의 장애 요인이 되었던 전원 문제나 통신 문제에 관해서도 다양한 해결책들이 제시되고 있음.

무선 기술의 발달은 센서 디바이스의 연결성을 획기적으로 높여주는 성과를 가져옴. 과거 특정한 센서 디바이스 설치를 위해 해당 위치까지 통신 회선을 가져가야 하는 엄청난 작업으로 비용적으로나 시간적으로 엄청난 부담을 주었으나, 현재는 연결에 대한 부담없이 센서 디바이스만 설치할 수 있는 기술적 발달로 센서 디바이스 설치의 제약 조건들을 획기적으로 줄어듬.

배터리 기술과 반도체 기술의 발달은 전원에 대한 제약 사항을 풀어 줌으로써, 완벽히 해결되지는 못했지만 전원 설치에 대한 부담을 현격히 줄어 들게 함.
이로 인해, 센서 디바이스의 설치에 대한 제약이 거의 사라져 원하는 곳 어디든 설치가 가능함.

스마트 환경 감시는 갈수록 나빠지고 있는 대기 환경이나 불규칙적인 날씨에 대한 효과적인 정보 제공이 가능하고, 위험 요소가 가득한 산업 현장에서 주위 환경 정보를 수집하고, 이로 부터 얻을 수 있는 다양한 정보로 보다 안전한 근무 환경 구성이 간능하도록 지원함.

### IoT 센서를 통한 데이터 수집

10여년만 하더라도 기상정보(강우, 풍속, 풍향, 등) 수집이나 특정 화학물질 감지를 위해서는 센서 디바이스 구입 및 설치를 위해서는 수백만원에서 1천만원 이상의 비용이 요구되었으나, 기술의 발달로 인해 수십만원의 저렴한 비용으로도 구축이 가능함.

### 환경 정보 제공

미세먼지 정보는 몇년전부터 대부분의 사람들이 관심을 가지고 있는 환경 정보로서 기상청이 설치한 몇개의 장소를 기준으로 제공됨.
설치 위치 또한 설치 공간적 문제로 인해 사람들이 생활하는 공간보다는 건물 옥상등의 약간은 동떨어진 위치가 대부분임.
이로 인한 환경 정보는 정보의 정확성이 제한적일 수 밖에 없었으나, 현재는 기술적 발달로 인해 최소한의 공간과 최소의 설치 비용만으로도 특정 지역의 환경 정보를 수집할 수 있어 소비자가 위치한 곳, 직접적으로 와 닿을 수 있는 곳의 정보를 세부적으로 제공할 수 있음.

### 데이터 분석을 통한 변화 예측

산업 현장에서의 환경 정보는 단순 참고 정보라기 보다는 사고 예방이라는 주요 목적을 가지지만, 일반적으로 이루어지는 예방 점검은 일정 기간 간격으로 특정 환경 정보에 대한 확인에 그침.
스마트 환경 감시는 반기/분기 등의 일정 기간 간격이 아닌 매일/매시간/매분 간격으로 지속적으로 감시할 수 있도록 지원함으로써, 환경에 대한 시간 기반의 변화 예측이 가능하도록 지원함.

### 사고 예방

지속적인 감시를 통한 변화의 감지는 문제가 발생하기 전에 사전 인지를 가져오고, 이를 통한 예방 조치로 사고를 방지할 수 있음.

## 기본 구성

스마트 환경 감시는 센서 디바이스, 게이트웨이, 서버로 기분 구성 가능함

![구성](./smart_environment_monitoring.svg)

### 센서 디바이스

- 환경 정보를 수집하기 위한 센서를 구동하고,
- 수집된 데이터를 서버로 직접적으로 전달하거나,
- 중간 게이트웨이로 전달해 데이터 수집이 가능하도록 함
- 센서 종류
  - 온도
  - 습도
  - 기체류 (CO2, CO, NH4, ...)
  - 액체류 (H2O, 나프타, ...)
- 데이터 전송을 위한 통신 방식에 따른 디바이스 종류
  - FDL20 Series
  - FDM20 Series
  - FDN20 Series

### 게이트웨이

- 센서 디바이스와 서버간 중간 연결자 역할
- 비 IP 기반 센서 디바이스의 인터넷 망 연결
- 통신 프로토콜 변환
  - Serial to IP
  - LPWA(LoRa, Wi-Sun, …) to IP
  - Digital I/O to IP, …
- 센서 디바이스 기능
- 게이트웨이 종류
  - FML50S Series
  - FTM80S Series

### 서버

- 센서 디바이스를 통해 수집된 데이터를 저장
- 데이터 가공을 통한 정보화
- 데이터 분석, 등

## 서비스

서비스 구성이 가능한 유형을 살펴 보고, 적용 가능 분야를 확인함.

### 수질 관리

- 지속적인 수질 관리가 필요한 곳에서 간헐적 감시가 아닌 지속적 감시를 통해 위험 요소에 의한 사고 예방이 가능함.
- 적용 분야
  - 워터파크
  - 집단 식수 설비
  - 산업 단지의 용수 등

### 인체에 유해한 고위험 물질 누출 감시

- 약간의 흡입이나 접촉등으로도 인체에 손상을 줄 수 있는 고위험 화학 물질 경우, 누출에 대한 감지가 사고의 규모에 직접적으로 영향을 줌.
- 지속적인 감시를 통한 짧은 시간의 사고 인지로 대형 사고로의 확산 방지
- 적용 분야
  - 대규모 화학공장
  - 가스 충전소
  - 방사능 물질 취급소 등

### 대규모 사업장에서의 안전 관리

- 중고업 사업장과 같이 넓은 지역에서 가스, 전기 등의 자원을 대량으로 소비함.
- 자원의 효율적인 관리 뿐만 아니라, 특정 자원의 누출(예를 들어, 가스)로 인한 사고 예방이 절실히 필요함.
- 수십년 된 사업장의 경우, 기반 통신 시설 부족에 따른 유선 장비 설치가 어려움.
- 무선 통신과 배터리 운영을 통한 설치의 최소화는 이러한 문제점을 해결하고, 지속적인 감시의 제공이 가능함.
- 적용 분야
  - 중장비 생산 공장
  - 조선소
  - 건설 공사장 등
