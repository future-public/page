---
# Display name
title: 센코

# Username (this should match the folder name)
authors:
- senko

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 센코
  url: "https://www.senko.co.kr/"

user_groups:
- "협력사"

---

![web](./web.png)