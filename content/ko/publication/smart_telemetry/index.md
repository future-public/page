---
title: 스마트 원격 측정
authors:
- xtra
date: "2020-08-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-08-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

#abstract: 대규모 지역이나 사업장에서의 다양한 자원 활용을 실시간으로 감시하고, 이에 대한 변화를 모니터링 함으로써 자원 활용의 효율을 극대화 시킬 뿐만 아나라 사용량 변화등의 감시를 통한 자원 낭비나 사고를 예방할 수 있음.

# Summary. An optional shortened abstract.
summary: 대규모 지역이나 사업장에서의 다양한 자원 활용을 실시간으로 감시하고, 이에 대한 변화를 모니터링 함으로써 자원 활용의 효율을 극대화 시킬 뿐만 아나라 사용량 변화등의 감시를 통한 자원 낭비나 사고를 예방할 수 있음.

tags:
#- Source Themes
featured: false

#links:
#- name: Custom Link
#  url: http://example.org
#url_pdf: http://arxiv.org/pdf/1512.04133v1
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Thomas Kelley**](https://unsplash.com/photos/xVptEZzgVfo)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: example
---

## 스마트 원격 측정이란?

수도 계량, 전기 계량, 가스 계량등은 우리에게 익숙한 단어들이며, 어떤 일은 하는지 누구나 알고 있음.
이러한 일들은 지속적인 확인 보다는 일정 기간 간격으로 확인하며, 지속적으로 반복됨.

고적적으로 지속되어오던 작업은 일반적으로 사람들에 의해 일일이 확인 과정이 진행되었으며,
이를 위한 비용 또한 만만치 않을 뿐만 아니라, 인건비 상승으로 인해 비용 부담 또한 증가됨.

스마트 원격 측정은 이러한 반복적 작업에 대해 자동화를 통해 최소한의 비용으로 최대한의 효율을 얻을 수 있도록 지원함.
또한, 정기적은 측정 뿐만 아니라 환경 변화에 따른 가변적을 측정이 가능하며, 인력에 의한 측정으로 발생할 수 있는 위험 요소나 불편 요소에 대해서도 대안이 될 수 있음.

## 특징

- 주기적 무인 측정
- 측정 과정에서 발생할 수 있는 사고 방지
- 자유로운 측정 주기 설정
- 저렴한 통신 비용 또는 자체망 구성을 통한 무제한 통신 가능
  - LPWA를 통한 로컬망 구
  - 로컬망과 공중망의 복합 구성

## 구성

## 서비스

주기적 측정이 가능한 대부분의 계량이 가능함.

### 용수 사용량 원격 측정

- 대규모 사업장에서 대용량으로 사용되는 산업 용수의 사용량에 대해 주기적 사용량 감시
- 임대를 목적으로 하는 다층 건물의 각 호실별 용수 사용량 측정

### 전력 사용량 원격 측정

- 전력 사용량이 많은 건물이나 사업장등에서 개별 요소별 전기 사용량 측정
- 여름철 전력 사용량 감시를 통한 과도한 냉방 감시
 
### 원격 출입 측정

- 특정 공간에 출입하는 인원수 측정
- 특정 시간대에 출입 인원 분석
