---
# Display name
title: 퓨쳐텍정보통신

# Username (this should match the folder name)
authors:
- futuretekict
- 

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 퓨쳐텍정보통신
  url: "https://www.ftkict.co.kr/"

user_groups:
- "협력사"

---

![web](./web.png)