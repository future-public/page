---
# Display name
title: 엔텍시스템

# Username (this should match the folder name)
authors:
- nteksys

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 엔텍시스템
  url: "https://nteksys.com/"

user_groups:
- "협력사"

---

![web](./web.png)