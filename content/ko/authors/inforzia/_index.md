---
# Display name
title: 인포지아

# Username (this should match the folder name)
authors:
- inforzia
- 

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 인포지아
  url: "https://inforzia.io/"

user_groups:
- "협력사"

---
