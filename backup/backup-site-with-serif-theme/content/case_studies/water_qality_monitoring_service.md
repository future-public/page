---
title: "Water Quality Monitoring Service"
date: 2018-11-28T15:15:26+10:00
draft: false
---

L사 워터파크 수질 감시 시스템

국내 최대 규모를 자랑하는 L사 워터파크의 수질을 실시간으로 측정하여 최적의 휴식 공간을 지원하기 위한 서비스를 제공한다.

L사 워터파크는 2019년 국내 최초 IoT 실시간 온라인 수질 관리 시스템을 도입하면서, 설비 구축을 위한 인프라 구성으로 LG U+와의 협력을 통한 무선 방식을 적용함.  

### 적용 디바이스

* FTM80-A
  * FTM80 Base
  * Analog Output 센서 연동
  * RS485 센서 연동

### 관련 기사

* 경남신문 - [올여름 안전한 물놀이는 '롯데워터파크'에서](http://www.knnews.co.kr/news/articleView.php?idxno=1329327)