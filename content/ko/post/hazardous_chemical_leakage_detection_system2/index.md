---
title: " L사 화학물질 누출 감시 시스템"
date: 2019-07-12
math: true
diagram: true
weight: 4
image:
  placement: 3
  caption: 'Image credit: [**Crystal Kwok**](https://unsplash.com/photos/xD5SWy7hMbw)'
---

> - 국내 탑 클래스의 화학 공장을 운영하고 있는 H사에서는 누출시 접축이나 폭발에 의한 사고 예방을 위해 화학 용액 누출 감시 시스템을 도입함.
> - 화학 생산 설비는 넓은 지역에 대규모 플랜트로 구성되어 있으며, 안전을 위한 다양한 제약 조건들이 존재해 유선망을 통한 신규 시스템 추가는 거의 불가능함.
> - 또한, 화학물질 생산 설비는 고위험 생산 설비로서 외부로 부터의 위험 요소에 대한 관리가 매우 엄격함

## 서비스 구성

![구성](./architecture.svg)

## 적용 기술

### 실시간 모니터링를 위한 서버 연동

- 실시간으로 수집된 데이터는 MQTT 프로토콜을 이용해 서버로 데이터
- L사에서 개발 운영 중인 서버 연동

### 전용 앱을 통한 센서 데이터 수집

- 자사에서 공급하는 센서 관리 전용 앱을 통해 센서 연동 

### 디지털 센서 연동

- 시리얼 포트를 통한 입력되는 사용자 정의 프로토콜 처리 

## 적용 디바이스

### FML50s

![FML50S](../../project/fml50s/fml50s.jpg)

- LTE 기반 무선 센서 게이트웨이
- 센서 데이터 수집을 위한 관리 프로그램 탑재
- RS485 포트를 통한 센서 연동
  
### 연동 센서

- Senko Analog Input Device
- Senko Digital Input Device
