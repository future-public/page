---
# Display name
title: 써닝포인트CC

# Username (this should match the folder name)
authors:
- sunningpoint

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 써닝포인트CC
  url: "https://www.sunningpoint.com/"

user_groups:
- "주요 고객사"

---

![web](./web.png)