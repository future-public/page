---
title: "H사의 VPN 무선 사설망을 이용한 물류망 구축"
date: 2019-07-12
math: true
diagram: true
weight: 1
image:
  placement: 3
  caption: 'Image credit: [**Alex Duffy**](https://unsplash.com/photos/a-E__y8y5Wo)'
---

> H사에서는 기존에 사용 중이던 무선 사설망을 저렴한 LTE 무선망으로 전환
> LTE 무선망 사용시 발생할 수 있는 외부로부터의 악의적 방해 요소 제거를 위해 VPN 망 구축
> 무선망의 안정적 운영을 위한 이중화 시스템 도입

---

## 서비스 구성

![서비스구성](./architecture.svg)

## 적용 기술

### 국가 공인 인증 보안 기술 적용

- CC(EAL4) 인증을 획득한 SSLVPN Client 탑재
- SSLVPN을 이용한 전용 사설망 구성

### 적용 디바이스

### FML50s

![FML50S](../../project/fml50s/fml50s.jpg)

- LTE 기반 무선 센서 게이트웨이
- SSLVPN Client

### WeGuardia SSLVPN

- HA 구조 적용(Active-Standby)
- CC(EAL4) 인증 획득
- 라우터 접속 관리
