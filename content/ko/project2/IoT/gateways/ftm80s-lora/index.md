---
title: FTM80s-LoRa
linktitle:
toc: true
type: docs
summary: 고성능 ARM 프로세서를 탑재하고 독립 운영이 가능한 LoRaWAN 게이트웨이 
tags:
- IoT Gateway
- LTE
- LoRa
date: "2016-04-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

menu:
  IoT:
    parent: IoT Gateways
    weight: 1
#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart
#
#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![FTM80s-LoRa](./featured.jpg)

> - 고성능 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터
> - 8채널 LoRa Concentrator를 내장하고 있는 무선-to-무선 게이트웨이

## Functions

### General

- Low power high performace CPU
- Linux support
- LTE Router
(LG U+ Certified)

### Networks

- Wired/Wireless failover
- Port fowarding
- Support for multiple protocols
  (TCP, UDP, SNMP, MQTT, HTTP,...)

### Management

- Web-based management tool
- Self reboot
(after checking communication status)
- Cloud-based health monitoring
(optional support)

### Security

- Netfilter
- SSLVPN client (Options)
(CC EAL4 certified)

### LoRa Gateway

- Up to 8 simultaneous channels
- Compliance to LoRaWAN Class A and Class C
- Integrated with multiple network server(NS)

### User-required programs can be loaded

- External device interworking
(Ethernet, RS232, RS485, Analog Input)
- Data Collection form device
- Tranfering data to a specified server
(MQTT, HTTP, user defined protocol)

## Specifications

|항목||내용|비고|
|-|-|:-:|---|
|CPU|Cortex-A8|1GHz||
|Memory|RAM|DDR3 512MB||
||Flash|SDHC 8GB ||
|Interface| LTE|B1/B5/B7||
|| 10/100/1000 Ethernet |2||
|| RS232 |1||
|| RS485 |1||
|| Analog Input|2||
|| Digial Input|2||
||LoRa Concentrator|SETALab module support|KC certified|
|LoRaWAN|Channel|8||
||Freqency Band|KR920,US902,AU915,AS923 and so on||
||Sensitivity|-140dBm @ 292bps||
||Protocol|V1.0 class A/C and V1.0.2 class A/C||
|Others|Operating temperature|-20 ~ 60||
||Storage temperature|-40 ~ 80||
||Power|12V ~ 24V DC||
||Dimensions|||
