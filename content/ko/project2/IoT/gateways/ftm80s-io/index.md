---
title: FTM80s-IO
linktitle:
toc: true
type: docs
summary: 고성능 ARM 프로세서를 탑재하고 다채널 입출력을 지원하는 I/O 게이트웨이
tags:
- IoT Gateway
- LTE
date: "2016-04-27T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

menu:
  IoT:
    parent: IoT Gateways
    weight: 1
#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![FTM80S-I0](./featured.jpg)

> - 고성능 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터
> - FTM80S에서 지원하는 2개의 확장 슬롯에는 최대 16 포트의 I/O 인터페이스가 지원
> - 인터페이스는 슬롯단위로 선택하거나 현장에 맞도록 주문 개발 가능

## Functions

### General

- Low power high performace CPU
- Linux support
- LTE Router
(LG U+ Certified)

### Networks

- Wired/Wireless failover
- Port fowarding
- Support for multiple protocols
  (TCP, UDP, SNMP, MQTT, HTTP,...)

### Management

- Web-based management tool
- Self reboot
(after checking communication status)
- Cloud-based health monitoring
(optional support)

### Security

- Netfilter
- SSLVPN client (Options)
(CC EAL4 certified)

### I/O Interface

- Digital Input/Output
(Digital Input/Output 8 ports)

### User-required programs can be loaded

- External device interworking
(Ethernet, RS232, RS485, Analog Input)
- Data Collection form device
- Tranfering data to a specified server
(MQTT, HTTP, user defined protocol)

## Specifications

|항목||내용|비고|
|-|-|:-:|---|
|CPU|Cortex-A8|1GHz||
|Memory|RAM|DDR3 512MB||
||Flash|SDHC 8GB ||
|Interface| LTE|B1/B5/B7||
|| 10/100/1000 Ethernet |2||
|| RS232 |1||
|| RS485 |1||
|| Analog Input|2||
|| Digial Input|2||
|Expantion Module| Digital I/O|4||
||Analog Input|4|To be supported|
|Others|Operating temperature|-20 ~ 60||
||Storage temperature|-40 ~ 80||
||Power|12V ~ 24V DC||
||Dimensions|||
