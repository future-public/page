---
# Display name
title: 두산인프라코어

# Username (this should match the folder name)
authors:
- doosaninfracore

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 두산인프라코어
- url: "https://www.doosaninfracore.com"

user_groups:
- "주요 고객사"

---

![Web](./web.png)