---
title: FML50s
summary: 저전력 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터.
tags:
- LTE
- IoT Gateway
date: "2016-04-27T00:00:00Z"
weight: 1

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

> 저전력 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터.
> 2개의 이더넷 포트를 지원함으로써 추가적인 이더넷 스위치 없이 로컬 네트워크 구성이 가능.
> 1개의 이더넷 포트와 LTE 통신을 이용한 failover 구성을 통해 안정적인 서비스 지원.

## Features

### General

* Low power CPU
* Linux support
* LTE Router
(LG U+ Certified)

### Networks

* Port forwarding
* Failover (Wired/Wireless)
* Support for multiple protocols
  (TCP, UDP, SNMP, MQTT, HTTP,...)

### Security

* SSLVPN Client
(CC EAL4 certified / optional)

### User-required programs can be loaded

* External device interworking
(Ethernet, RS232, RS485)
* Data Collection form device
* Tranfering data to a specified server

## Specifications

|항목||내용|비고|
|-|-|:-:|---|
|CPU|ARM9|333MHz||
|Memory|DRAM|DDR2 128MB||
||Flash|SDHC 4GB ||
|Interface|Ethernet|2 x 10/100Mbps||
||Serial |1 x RS232||
|||1 x RS485||
|Others|Operating temperature|-20°C ~ 60°C||
||Storage temperature|-40 °C ~ 80 °C||
||Dimensions|||
||Power|12V DC||
