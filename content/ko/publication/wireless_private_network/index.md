---
title: 무선 사설망 구축
authors:
- xtra
date: "2020-08-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-08-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

#abstract: 운영 환경의 특성(지역적 분포, 위험 시설물, 이동체)상 유선 설비의 구축이 어렵고, 악의적인 외부 요인으로부터의 보호가 반드시 필요한 산업 현장에 대해, 무선 기술과 국기 인증 보안 기술을 통한 해결 방안 제시

# Summary. An optional shortened abstract.
summary: 운영 환경의 특성(지역적 분포, 위험 시설물, 이동체)상 유선 설비의 구축이 어렵고, 악의적인 외부 요인으로부터의 보호가 반드시 필요한 산업 현장에 대해, 무선 기술과 국기 인증 보안 기술을 통한 해결 방안 제시

tags:
#- Source Themes
featured: false

#links:
#- name: Custom Link
#  url: http://example.org
#url_pdf: http://arxiv.org/pdf/1512.04133v1
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Jan Antonin Kolar**](https://unsplash.com/photos/-LVYB8ZdAb0)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: example
---

## LTE 무선 사설망이란?

LTE 무선 사설망이란, LTE 무선 통신 이용해 외부의 영향을 받지 않는 독자적인 망 구축을 말함.

LTE 무선 사설망 구축으로는 아래와 같은 2가지 방안이 있음.

- Private LTE를 이용한 무선 사설망 구축
- VPN 이용한 사설망 구축

### Private LTE 사설망

Private LTE 사설망은 LTE 통신 서비스를 제공하는 통신 서비스로서, LTE 접속에 필요한 USIM을 별도 발급 받아 사용하며 통신사와 망공급 계약 체결이 필요함

### VPN을 이용한 사설망

VPN을 사설망 구축은 LTE 공용망을 사용하지만, VPN 서비스를 이용해 별도의 사설망을 구축함. 이 경우, 통신사의 계약이 필요 없으며, 최소 사용량 등의 보장이 필요하지 않음.

## 구성

![구성](./wireless_private_network.svg)

## 적용 예

### 대규모 이동 설비를 위한 사설망 구축

항만물류등과 같이 다수의 이동체들이 통신의 주체가 될 경우, 무선 통신을 통한 망 구성은 필수 요건이며, 와이파이 등을 통한 망 구성이 주를 이루었으나 거리 제약등의 단점을 가지고 있음.
LTE의 보급이 확대되면서 이러한 물류시스템에도 와이파이와 같은 단거리 통신에서 LTE등의 장거리 통신으로 전환되고 있으며, 관련 망의 보안을 위해 사설망 구축이 필요함.

### 관제 설비 관리를 위한 사설망

농어촌공사등에서 관리되고 있는 저수시설은 용수의 사용에 따라 수문을 열거나 닫아야 함. 이러한 설비 제어는 외부의 악의적 조작에 대한 보호를 위해 사설망 구축이 요구됨.

### CCTV 사설망

시설물 관리를 위해 사용되는 CCTV를 위해서는 일반적으로 유선이 사용되었지만, 최근들어 LTE 무선 통신의 사용이 증가되고 있음. 이는, CCTV의 활용이 건물이나 좁은 지역적 범위를 넘어서 수십~수백km까지 확대됨.
또한, CCTV의 영상 유출 방지를 위해 사설망 구축이 요구됨.