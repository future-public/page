---
# Display name
title: 달리웍스

# Username (this should match the folder name)
authors:
- daliworks

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: Daliworks
  url: "https://www.daliworks.net/"

user_groups:
- "협력사"

---

![Avatar](./web.png)