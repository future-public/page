---
title: FTM80s
linktitle: FTM80s
toc: true
type: docs
summary: 고성능 ARM 프로세서를 탑재하고 있는 LTE 기반의 산업용 라우터
tags:
#- IoT Gateway
#- LTE
date: "2018-11-01T00:00:00Z"

menu:
  Router:
    parent: LTE Routers
    weight: 2

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
# slides: example
---

![FTM80S](./featured.jpg)

> - 고성능 ARM 프로세서를 탑재하고 있는 LTE 기반 산업용 라우터
> - 2개의 확장 슬롯이 지원으로 현장에 맞는 다양한 인터페이스 구성 가능
> - 인터페이스 모듈은 유선 뿐만 아니라 무선 통신을 이용한 사설 무선망 구성 가능

## General

- Low power high performace CPU
- Linux support
- LTE Router
(LG U+ Certified)

## Networks

- Wired/Wireless failover
- Port fowarding
- Support for multiple protocols
  (TCP, UDP, SNMP, MQTT, HTTP,...)

## Management

- Web-based management tool
- Self reboot
(after checking communication status)
- Cloud-based health monitoring
(optional support)

## Security

- Netfilter
- SSLVPN client (Options)
(CC EAL4 certified)

### Expantion Module

- LoRa Concentrator  
  - Seta Lab Inc. module support
  - Up to 8 simultaneous channels
  - Compliance to LoRaWAN Class A and Class C
  - Integrated with multiple network server(NS)
- Digital Input/Output  
  - Max 8 channels
- Analog Input/Output  
  - Max 8 channels
- Sub-1GHz Concentrator
  - Ti SimpleLink


### User-required programs can be loaded

- External device interworking
(Ethernet, RS232, RS485, Analog Input)
- Data Collection form device
- Tranfering data to a specified server
(MQTT, HTTP, user defined protocol)

## Specifications

|항목||내용|비고|
|-|-|:-:|---|
|CPU|Cortex-A8|1GHz||
|Memory|RAM|DDR3 512MB||
||Flash|SDHC 8GB ||
|Interface| LTE|B1/B5/B7||
|| 10/100/1000 Ethernet |2||
|| RS232 |1||
|| RS485 |1||
|| Analog Input|2||
|| Digial Input|2||
|Expantion Module| Digital I/O|4||
||Analog Input|4|To be supported|
||LoRa Concentrator|SETALab module support|KC certified|
|LoRaWAN|Channel|8||
||Freqency Band|KR920,US902,AU915,AS923 and so on||
||Sensitivity|-140dBm @ 292bps||
||Protocol|V1.0 class A/C and V1.0.2 class A/C||
|Others|Operating temperature|-20 ~ 60||
||Storage temperature|-40 ~ 80||
||Power|12V ~ 24V DC||
||Dimensions|||
