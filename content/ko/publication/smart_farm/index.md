---
title: 스마트 농업
authors:
- xtra
date: "2020-08-01T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2020-08-0 1T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

#abstract: 실시간 환경 모니터링 및 원격 제어를 통해 농작물의 성장을 촉진 시키고, 생산품의 품질을 향상 시킴으로써, 생산자의 소득 증대를 가져옴.

# Summary. An optional shortened abstract.
summary: 실시간 환경 모니터링 및 원격 제어를 통해 농작물의 성장을 촉진 시키고, 생산품의 품질을 향상 시킴으로써, 생산자의 소득 증대를 가져옴

tags:
# - Source Themes
featured: false

#links:
#- name: Custom Link
#  url: http://example.org
#url_pdf: http://arxiv.org/pdf/1512.04133v1
#url_code: '#'
#url_dataset: '#'
#url_poster: '#'
#url_project: ''
#url_slides: ''
#url_source: '#'
#url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
#slides: example
---


## 스마트 팜이란?

스마트 팜이란 비닐하우스·축사에 ICT를 접목하여 원격·자동으로 작물과 가축의 생육환경을 적정하게 유지·관리할 수 있는 농장을 말함.

작물의 재배에는 온도/습도/광량등 다양한 환경 요소들의 조화가 중요함.
작물의 특성에 따라 적절한 온도를 유지하지 못하면, 작물의 성장에 지장을 줄 뿐만 아니라 심할 경우 한해 농사를 모두 망칠 수 있는 치명적인 손상을 가져 올 수 있음.
스마트 팜은 IT 기술을 이용해 작물의 성장에 최적화 될 수 있는 환경을 제공할 수 있도록 지원하여 최상의 품질로 최대의 수익을 가져올 수 있도록 지원함.

스마트 팜의 또다른 이점은 사업자에게 제한적 자유를 부여할 수 있음. 사업자는 작물을 재해하는 동안 작물 재배지 이탈이 매우 힘듦. 이는 작물 재배시 환경 변화로 인한 손실이 발생할 수 있기 때문으로, 스마트 팜의 IT 기술은 이러한 문제에 대한 해결책 제시가 가능함.

스마트 팜은 센싱과 제어로 구성될 수 있으며, 센싱은 환경 감시를 주 목적으로 하고, 제어는 원격지에서나 대규모 농장에서의 설비 제어를 목적으로 함.

## 기대 효과

- ICT를 접목한 스마트 팜이 보편적으로 확산되면 노동·에너지 등 투입 요소의 최적 사용을 통해 우리 농업의 경쟁력을 한층 높이고,미래성장산업으로 견인 가능
- 단순한 노동력 절감 차원을 넘어서 농작업의 시간적·공간적 구속으로부터 자유로워져 여유시간도 늘고, 삶의 질도 개선되어 우수 신규인력의 농촌 유입 가능성도 증가할 것으로 기대

## 구성

### 하우스 작물 관리

하우스에서 재배되는 작물은 각각의 특성에 맞는 환경 유지가 필요함. 작물에 따른 적정 온도와 광량이 성장에 중요한 요소가 되며, 이는 재배자의 소득에 직접적인 영향을 줄 수 있는 요인이 됨.
스마트 팜은 이러한 재배환경 최적화 지원이 가능하며, 원격 제어등을 통해 재배자의 활동 범위를 넓혀줄 수 있음.

![하우스](./green_house.jpg)
> 교체 예정

### 수직 창고형 식물 공장

수직 창고형 식물 공장은 자연적 환경이 아닌 인공적 환경하에서 인위적 환경하에서 작물을 재배해 최대한의 이득을 얻을 수 있는 대량 생산형 시스템을 말함.
이러한 인공 재배 환경에서는 환경 제어가 매우 중요하며, 재배 설비와 재배 작물에 맞는 적절한 제어가 필수적으로 요구됨.

![수직 창고형 식물 공장](./vertical_smart_farm.jpg)
> 교체 예정

### 축사 관리

좁은 공간에 다수의 가축을 키우고 있는 현재의 축산 시스템에서는 축사 내 환경 유지가 건강한 가축을 키우는데 매우 중요한 요인이 됨. 
비 위생적으로 방치되는 축사의 경우, 전염병에 매우 취학한 것 뿐만 아니라, 불쾌한 환경은 가축들에게 스트레스를 줌으로서 우수한 축산물 생산에 방해 요소가 됨.
IoT 기술은 이러한 축사 환경을 감시하고 제어함으로서 가축 생육에 도움을 줄 수 있는 최적의 환경이 유지될 수 있도록 지원이 가능함.

![스마트 축사](./smart_pig_farm.png)
> 교체 예정
