---
title: Telemetry Service
authors:
- admin
date: "2019-04-07T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["4"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: 대규모 지역이나 사업장에서의 다양한 자원 활용을 실시간으로 감시하고, 이에 대한 변화를 모니터링 함으로써 자원 활용의 효율을 극대화 시킬 뿐만 아나라 사용량 변화등의 감시를 통한 자원 낭비나 사고를 예방할 수 있음.

# Summary. An optional shortened abstract.
summary: 대규모 지역이나 사업장에서의 다양한 자원 활용을 실시간으로 감시하고, 이에 대한 변화를 모니터링 함으로써 자원 활용의 효율을 극대화 시킬 뿐만 아나라 사용량 변화등의 감시를 통한 자원 낭비나 사고를 예방할 수 있음.

tags:
- Source Themes
featured: false

links:
- name: Custom Link
  url: http://example.org
url_pdf: http://arxiv.org/pdf/1512.04133v1
url_code: '#'
url_dataset: '#'
url_poster: '#'
url_project: ''
url_slides: ''
url_source: '#'
url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- ftm80s

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: example
---

대규모 지역이나 사업장에서의 다양한 자원 활용을 실시간으로 감시하고, 이에 대한 변화를 모니터링 함으로써 자원 활용의 효율을 극대화 시킬 뿐만 아나라 사용량 변화등의 감시를 통한 자원 낭비나 사고를 예방할 수 있음.

## 고려 사항

## 구성

## 적용 예
