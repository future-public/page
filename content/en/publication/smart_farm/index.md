---
title: 'Smart Farm Service'
authors:
- admin
date: "2019-04-07T00:00:00Z"
doi: ""

# Schedule page publish date (NOT publication's date).
publishDate: "2017-01-01T00:00:00Z"

# Publication type.
# Legend: 0 = Uncategorized; 1 = Conference paper; 2 = Journal article;
# 3 = Preprint / Working Paper; 4 = Report; 5 = Book; 6 = Book section;
# 7 = Thesis; 8 = Patent
publication_types: ["0"]

# Publication name and optional abbreviated publication name.
publication: ""
publication_short: ""

abstract: 실시간 환경 모니터링 및 원격 제어를 통해 농작물의 성장을 촉진 시키고, 생산품의 품질을 향상 시킴으로써, 생산자의 소득 증대를 가져옴.

# Summary. An optional shortened abstract.
summary: 실시간 환경 모니터링 및 원격 제어를 통해 농작물의 성장을 촉진 시키고, 생산품의 품질을 향상 시킴으로써, 생산자의 소득 증대를 가져옴

tags:
- Source Themes
featured: false

links:
- name: Custom Link
  url: http://example.org
url_pdf: http://arxiv.org/pdf/1512.04133v1
url_code: '#'
url_dataset: '#'
url_poster: '#'
url_project: ''
url_slides: ''
url_source: '#'
url_video: '#'

# Featured image
# To use, add an image named `featured.jpg/png` to your page's folder.
image:
  caption: 'Image credit: [**Unsplash**](https://unsplash.com/photos/s9CC2SKySJM)'
  focal_point: ""
  preview_only: false

# Associated Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter your project's folder or file name without extension.
#   E.g. `internal-project` references `content/project/internal-project/index.md`.
#   Otherwise, set `projects: []`.
projects:
- internal-project

# Slides (optional).
#   Associate this publication with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides: "example"` references `content/slides/example/index.md`.
#   Otherwise, set `slides: ""`.
slides: example
---

실시간 환경 모니터링 및 원격 제어를 통해 농작물의 성장을 촉진 시키고, 생산품의 품질을 향상 시킴으로써, 생산자의 소득 증대를 가져옴

## 고려 사항

스마트팜은 구성시 다음과 같은 사항에 대해 고려가 필요하다.

* 넓은 공간
* 전원 공급
* 자유로운 설치 및 제거

## 구성

## 적용 예
