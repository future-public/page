---
title: Case Studies
subtitle: In the context of real-world applications

columns: 2

# View.
#   1 = List
#   2 = Compact
#   3 = Card
view: 2

# Optional header image (relative to `static/img/` folder).
header:
  caption: ""
  image: "future/banners/arm_iot.jpg"
---
