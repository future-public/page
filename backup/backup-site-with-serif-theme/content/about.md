---
title: 'About'
menu: 'main'
---

Future ICT is a technology group develops, produces and sells [sensor nodes and gateways](/products), implementing connectivity for Industrial IoT ("Industry 4.0") applications. 

Future ICT is an [enginneering consultancy](/case_studies) company specialized in prototyping IoT devices, as a reference, tailored to specific [industrial application](/industries) contexts.

Our engineering efforts are mainly aimed at improving:
  - Security in wireless communication
  - Reliability in harsh environments
  - Openness in platforms

The company was founded in 2019 by a few of experts in the embedded fields for secure communication.
