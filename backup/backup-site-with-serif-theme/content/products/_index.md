---
title: 'Products'
intro_image: images/future/banners/products.svg
intro_image_absolute: false
intro_image_hide_on_mobile: true
---

# IoT Gateways & Sensor Nodes
## Simplify the networking of things.

현실과 가상을 연결해주는 중간 매개체로서 현실로부터 수집된 데이터들을 다양한 방법으로 가공하고 전달하여 가상의 공간에서 현실을 볼 수 있도록 지원한다.
