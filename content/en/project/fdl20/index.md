---
title: FDL20-TH
summary: LoRa 통신 기반의 배터리 운영이 가능한 저전력 무선 통신 디바이스
tags: 
- IoT Sensor Node
date: "2020-07-18T12:33:46+10:00"

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: ""
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

> - LoRa 통신 기반의 배터리 운영이 가능한 저전력 디바이스.
> - 다양한 인터페이스 지원을 통해 각종 센서와 연동되어 있고, 이를 통해 수집된 데이터는 저전력 장거리 통신을 통해 서버로 전달.  
> - 배터리 운영을 통한 간편한 설치와 고객에 맞춘 최적화 작업을 통해 기존 설비에 데이터 수집 지원.

## Key Features

- Ultra Low Power & High Performance Microcontroller
- Supprt LoRaWAN 1.0.2 (Class A/C)
- Frequency : 902 ~ 958MHz
- AES-128 encryption/decryption
- Max Current: 30~40mA(TX, 10dbm), 10mA(Listen), 10uA(Sleep)
  
## Applications

- Wireless sensor Network and Security System.
- Smart Utility.
- Smart Metering.
- Home and Building Automation.
- Process and Building Control.  

## Supported Sensors

|Type|Model|Functions|Range|Comment|
|---|---|---|:-:|---|
|Multi-Fnctions|SHT3x|Temperautre(°C)|-40 ~ 120|Digital Sensor|
|||Humidity(%RH)|0 ~ 100||
|Temperature|DS18B20|Temperature(°C)|-40 ~ 120|Digital Sensor|
|Digital Input|-|Dry contact||Digital Sensor|
|Analog Input|-|Voltage(V)|0~3.3|Digital Sensor|

## Specifications

|항목||내용|비고|
|---|---|:-:|---|
|CPU|Cortex-M0|32MHz||
|Memory|RAM|20KB||
||Flash|192KB||
|Interface| Serial|1 x RS232|Option|
|||1 x RS485|Option|
|||1 x I2C| Option|
||Input/Output|2 x AI|Option|
|||2 x DI|Option|
|LoRa|Class|Class A, Class C||
||RF Region|KR920|
||RF Power|14dBm|
|Others|Operating temperature|-20℃ ~ 60℃| |
||Storage temperature|-40℃ ~ 80℃| |
||Dimensions|||
||Power|Lithium Battery 3.6V (D Size)||
