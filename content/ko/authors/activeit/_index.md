---
# Display name
title: 액티브아이티

# Username (this should match the folder name)
authors:
- activeit

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: 액티브아이티
  url: "https://lguplusit.co.kr/"

user_groups:
- "협력사"

---

![Avatar](./web.png)