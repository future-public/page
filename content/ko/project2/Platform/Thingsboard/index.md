---
title: Thingsboard
linktitle: Thingsboard
toc: true
type: docs
summary: Thingsboard
tags:
#- IoT Gateway
#- LTE
date: "2016-04-27T00:00:00Z"

menu:
  Platform:
    parent: Service Platform
    weight:  4

# Optional external URL for project (replaces project detail page).
external_link: ""

#image:
#  caption: Photo by rawpixel on Unsplash
#  focal_point: Smart

#links:
#- icon: twitter
#  icon_pack: fab
#  name: Follow
#  url: https://twitter.com/georgecushen
#url_code: ""
#url_pdf: ""
#url_slides: ""
#url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
#slides: example
---

![Thingsboard](./thingsboard.png)

> -  Open-source IoT platform for device management, data collection, processing and visualization for your IoT projects

## General

