---
# Display name
title: 한화토탈

# Username (this should match the folder name)
authors:
- hanwhatotal

# Is this the primary user of the site?
superuser: false

# Role/position
role:

# Organizations/Affiliations
organizations:
- name: HanwhaTotal
  url: "https://www.hanwha-total.com"

user_groups:
- "주요 고객사"

---

![Avatar](./web.png)